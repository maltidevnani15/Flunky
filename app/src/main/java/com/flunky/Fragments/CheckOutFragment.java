package com.flunky.Fragments;

import android.Manifest;
import android.app.DatePickerDialog;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.icu.text.DecimalFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.flunky.Activities.MainActivity;
import com.flunky.FlunkyApplication;

import com.flunky.Model.RestaurantSortMenuItemsModel;
import com.flunky.Model.UserResponseModel;
import com.flunky.R;
import com.flunky.imagepicker.ImagePicker;
import com.flunky.imagepicker.ImagePickerInterface;
import com.flunky.utils.Constant;
import com.flunky.utils.Logger;
import com.flunky.utils.TabType;
import com.flunky.utils.Utils;
import com.flunky.webservices.ApiConstant;
import com.flunky.webservices.ApiInterface;
import com.flunky.webservices.RestClient;
import com.flunky.webservices.RetrofitCallback;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;


import static android.R.attr.data;
import static android.app.Activity.RESULT_OK;

public class CheckOutFragment extends BaseFragment implements ImagePickerInterface, ImagePicker.OnGetBitmapListener, View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    private LinearLayout fragment_checkout_ll_iv_first,fragment_checkout_ll_iv_first1,fragment_checkout_ll_iv_first2;
    private LinearLayout fragment_checkout_ll_iv_second;

    private TextView fragment_checkout_tv_contactName;
    private TextView fragment_checkout_tv_occasion;
    private TextView fragment_checkout_tv_select_occasion;
    private TextView fragment_checkout_tv_select_value;
    private TextView fragment_checkout_tv_coupan_value;
    private TextView fragment_checkout_tv_purchase;

    private EditText fragment_checkout_et_message;

    private RadioGroup fragment_checkout_rg_send;

    private RadioButton fragment_checkout_rb_send_date;

    private ImageView fragment_checkout_iv_first,fragment_checkout_iv_first1,fragment_checkout_iv_first2;
    private ImageView fragment_checkout_iv_second;

    private ImagePicker imagePicker;
    private Uri uri = null;

    private static final int REQUEST_CODE_PICK_CONTACTS = 20;
    private Uri uriContact;
    private String contactNo;

    private String occassion_id;
    private String gift_amount_id;
    private String rest_id,rest_img,rest_img1,rest_img2,imagePath,rest_name;
    private String is_now="1";

    private SimpleDateFormat dateSimpleDateFormat;
    private Calendar dateTimeCalendar;
    private Calendar todayCalendar;
    private Date date;
    DecimalFormat df;

    private long mLastClickTime = 0;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_checkout, container, false);
    }


    @Override
    protected void initView(View view) {

         df = new DecimalFormat(".00");

        if (getArguments() != null) {
            rest_id = getArguments().getString("rest_id");
            rest_img = ApiConstant.BASE_URL +getArguments().getString("rest_img");
            rest_img1= ApiConstant.BASE_URL +getArguments().getString("rest_img1");
            rest_img2 = ApiConstant.BASE_URL +getArguments().getString("rest_img2");
            rest_name=getArguments().getString("rest_name");
        }

        imagePath=rest_img;
        date = new Date();
        imagePicker = new ImagePicker(getContext(), this);
        dateTimeCalendar = Calendar.getInstance();
        todayCalendar = Calendar.getInstance();
        dateSimpleDateFormat = new SimpleDateFormat(Constant.EXPECTED_DATE_FORMATTER, Locale.getDefault());

        fragment_checkout_ll_iv_first=(LinearLayout)view.findViewById(R.id.fragment_checkout_ll_iv_first);
        fragment_checkout_ll_iv_first1=(LinearLayout)view.findViewById(R.id.fragment_checkout_ll_iv_first1);
        fragment_checkout_ll_iv_first2=(LinearLayout)view.findViewById(R.id.fragment_checkout_ll_iv_first2);

        fragment_checkout_ll_iv_second=(LinearLayout)view.findViewById(R.id.fragment_checkout_ll_iv_second);


        fragment_checkout_tv_contactName = (TextView) view.findViewById(R.id.fragment_checkout_tv_contactName);
        fragment_checkout_tv_occasion = (TextView) view.findViewById(R.id.fragment_checkout_tv_occasion);
        fragment_checkout_tv_select_occasion=(TextView)view.findViewById(R.id.fragment_checkout_tv_select_occasion);
        fragment_checkout_tv_select_value=(TextView)view.findViewById(R.id.fragment_checkout_tv_select_value);
        fragment_checkout_tv_coupan_value = (TextView) view.findViewById(R.id.fragment_checkout_tv_value);
        fragment_checkout_tv_purchase = (TextView) view.findViewById(R.id.fragment_checkout_tv_purchase);
        fragment_checkout_rg_send = (RadioGroup) view.findViewById(R.id.fragment_checkout_rg_send);

        fragment_checkout_rb_send_date = (RadioButton) view.findViewById(R.id.fragment_checkout_rb_select_date);
        fragment_checkout_et_message = (EditText) view.findViewById(R.id.fragment_checkout_et_message);

        fragment_checkout_iv_first = (ImageView) view.findViewById(R.id.fragment_checkout_iv_first);
        fragment_checkout_iv_first1 = (ImageView) view.findViewById(R.id.fragment_checkout_iv_first1);
        fragment_checkout_iv_first2 = (ImageView) view.findViewById(R.id.fragment_checkout_iv_first2);
        fragment_checkout_iv_second=(ImageView)view.findViewById(R.id.fragment_checkout_iv_second);

        fragment_checkout_ll_iv_first.setOnClickListener(this);
        fragment_checkout_ll_iv_first1.setOnClickListener(this);
        fragment_checkout_ll_iv_first2.setOnClickListener(this);
        fragment_checkout_ll_iv_second.setOnClickListener(this);
        fragment_checkout_tv_contactName.setOnClickListener(this);

        fragment_checkout_tv_select_occasion.setOnClickListener(this);
        fragment_checkout_tv_select_value.setOnClickListener(this);

        fragment_checkout_tv_purchase.setOnClickListener(this);
        fragment_checkout_rg_send.setOnCheckedChangeListener(this);



        Picasso.with(getContext()).load(rest_img).fit().into(fragment_checkout_iv_first);

        if (rest_img1.equals(ApiConstant.BASE_URL)){
            fragment_checkout_ll_iv_first1.setVisibility(View.GONE);
        }else {
            fragment_checkout_ll_iv_first1.setVisibility(View.VISIBLE);
            Picasso.with(getContext()).load(rest_img1).fit().into(fragment_checkout_iv_first1);
        }

        if (rest_img2.equals(ApiConstant.BASE_URL)){
            fragment_checkout_ll_iv_first2.setVisibility(View.GONE);
        }else {
            fragment_checkout_ll_iv_first2.setVisibility(View.VISIBLE);
            Picasso.with(getContext()).load(rest_img2).fit().into(fragment_checkout_iv_first2);
        }
        ArrayList<RestaurantSortMenuItemsModel> occassionList;
        occassionList = Utils.getArrayList(getContext(), Constant.EVENTS_LIST);
        fragment_checkout_tv_occasion.setText(occassionList.get(0).getValue());


        ArrayList<RestaurantSortMenuItemsModel> coupanList;
        coupanList = Utils.getArrayList(getContext(), Constant.COUPAN_PRICE);

        fragment_checkout_tv_coupan_value.setText("$ "+df.format(Double.parseDouble(coupanList.get(0).getValue())));

    }

    @Override
    protected void initToolbar() {
        MainActivity.getInstance().secondTabTitle().setText(R.string.check_out);
        MainActivity.getInstance().secondTabSubTitle().setText(R.string.send_gift);
        MainActivity.getInstance().walletClick().setOnClickListener(null);
        MainActivity.getInstance().calenadrClick().setOnClickListener(null);

    }

    public void openImagePicker(View view) {

        imagePicker.setOnGetBitmapListener(this);
        final int finePermissionCheck = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        if (finePermissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) ;
            {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constant.REQUEST_CODE_ASK_PERMISSIONS);
            }
        } else {
            imagePicker.createImageChooser();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == ImagePicker.GALLERY_REQUEST) {
            uri = data.getData();
            imagePicker.onActivityResult(requestCode, uri);
        }
        if (resultCode == RESULT_OK && requestCode == ImagePicker.CAMERA_REQUEST) {
            imagePicker.onActivityResult(requestCode, uri);
        }
        if (requestCode == REQUEST_CODE_PICK_CONTACTS && resultCode == RESULT_OK) {
            uriContact = data.getData();
            retrieveContactName();
        }
        if (requestCode == 100) {
            fragment_checkout_tv_occasion.setText(data.getStringExtra("Data"));
            occassion_id=data.getStringExtra("id");
            Log.e("Data", data.getStringExtra("Data"));
        }
        if (requestCode == 1) {

            fragment_checkout_tv_coupan_value.setText("$ "+df.format(Double.parseDouble(data.getStringExtra("Data"))));
//            fragment_checkout_tv_coupan_value.setText(getResources().getString(R.string.dollar)+data.getStringExtra("Data"));
            gift_amount_id=data.getStringExtra("id");
            Log.e("Data", data.getStringExtra("Data"));
        }

    }


    @Override
    public void onGetBitmap(Bitmap bitmap) {
        if (bitmap != null) {
            fragment_checkout_ll_iv_second.setBackgroundResource(R.drawable.image_selector);
            fragment_checkout_ll_iv_first.setBackgroundResource(0);
            fragment_checkout_ll_iv_first1.setBackgroundResource(0);
            fragment_checkout_ll_iv_first2.setBackgroundResource(0);
            fragment_checkout_iv_second.setImageBitmap(bitmap);
        }
    }

    @Override
    public void handleCamera(Intent takePictureIntent) {
        uri = Uri.fromFile(new File(imagePicker.createOrGetProfileImageDir(getContext()), System.currentTimeMillis() + ".jpg"));
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(takePictureIntent, ImagePicker.CAMERA_REQUEST);
    }

    @Override
    public void handleGallery(Intent galleryPickerIntent) {
        startActivityForResult(galleryPickerIntent, ImagePicker.GALLERY_REQUEST);
    }


    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 500) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        TabType tabType = null;
        switch (v.getId()) {

            case R.id.fragment_checkout_tv_edit_send_date:
                new DatePickerDialog(getContext(), onSendDateSetListener, dateTimeCalendar.get(Calendar.YEAR),
                        dateTimeCalendar.get(Calendar.MONTH), dateTimeCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.fragment_checkout_tv_contactName:
                setContactData();
                break;

            case R.id.fragment_checkout_tv_select_occasion:
                tabType = TabType.OCCASSION;
                setOccasionDialog();
                break;
            case R.id.fragment_checkout_tv_select_value:
                tabType = TabType.COUPANPRICE;
                setValueDialog();
                break;

            case R.id.fragment_checkout_ll_iv_first:
                imagePicker.setImagePath();
                imagePath=rest_img;
                fragment_checkout_ll_iv_first.setBackgroundResource(R.drawable.image_selector);
                fragment_checkout_ll_iv_first2.setBackgroundResource(0);
                fragment_checkout_ll_iv_first1.setBackgroundResource(0);
                fragment_checkout_ll_iv_second.setBackgroundResource(0);
                break;
            case R.id.fragment_checkout_ll_iv_first1:
                imagePicker.setImagePath();
                imagePath=rest_img1;
                fragment_checkout_ll_iv_first1.setBackgroundResource(R.drawable.image_selector);
                fragment_checkout_ll_iv_first2.setBackgroundResource(0);
                fragment_checkout_ll_iv_first.setBackgroundResource(0);
                fragment_checkout_ll_iv_second.setBackgroundResource(0);
                break;
            case R.id.fragment_checkout_ll_iv_first2:
                imagePicker.setImagePath();
                imagePath=rest_img2;
                fragment_checkout_ll_iv_first2.setBackgroundResource(R.drawable.image_selector);
                fragment_checkout_ll_iv_first1.setBackgroundResource(0);
                fragment_checkout_ll_iv_first.setBackgroundResource(0);
                fragment_checkout_ll_iv_second.setBackgroundResource(0);
                break;
            case R.id.fragment_checkout_ll_iv_second:

                openImagePicker(v);
                break;
            case R.id.fragment_checkout_tv_purchase:
                doValidation();
                break;
        }
        FlunkyApplication.getInstance().setTabType(tabType);
    }

    private void setValueDialog() {
        ArrayList<RestaurantSortMenuItemsModel> coupanList;
        final Bundle b = new Bundle();
        coupanList = Utils.getArrayList(getContext(), Constant.COUPAN_PRICE);
        b.putParcelableArrayList("dataList", coupanList);


        final EditDataDialog coupandialogFragment = new EditDataDialog();
        coupandialogFragment.setArguments(b);
        coupandialogFragment.setTargetFragment(this, 1);
        coupandialogFragment.show(getFragmentManager().beginTransaction(), coupandialogFragment.getClass().getSimpleName());


    }

    private void setOccasionDialog() {
        ArrayList<RestaurantSortMenuItemsModel> occassionList;
        final Bundle bundle = new Bundle();
        occassionList = Utils.getArrayList(getContext(), Constant.EVENTS_LIST);
        bundle.putParcelableArrayList("dataList", occassionList);
        final EditDataDialog dialogFragment = new EditDataDialog();


            dialogFragment.setArguments(bundle);
            dialogFragment.setTargetFragment(this, 100);
            dialogFragment.show(getFragmentManager().beginTransaction(), dialogFragment.getClass().getSimpleName());



    }

    private void doValidation() {
        final String receiverName = fragment_checkout_tv_contactName.getText().toString();
        final String occassion = fragment_checkout_tv_occasion.getText().toString();
        final String value = fragment_checkout_tv_coupan_value.getText().toString();
        final String occassionDate="";
        if(!receiverName.equalsIgnoreCase("Select Contact")){
            if(!occassion.equalsIgnoreCase("Select Occasion")){
                if(!value.equalsIgnoreCase("Select value")){
                        if(!is_now.equalsIgnoreCase("none")){
                           navigateToNextFragment(occassionDate,receiverName);
                        }else{
                            Logger.showSnackBar(getContext(),getString(R.string.select_any_one_option));
                        }

                }else{
                   Logger.showSnackBar(getContext(),getString(R.string.select_value));
                }
            }else{
                Logger.showSnackBar(getContext(),getString(R.string.select_occassion));
            }
        }else{
            Logger.showSnackBar(getContext(),getString(R.string.select_contact));
        }


    }

    private void navigateToNextFragment(String occasionDate, String receiverName) {
         final String sendDate;
        Bundle  b=new Bundle();
        b.putString("receiverName",receiverName);
        b.putString("receiverPhone",contactNo);
        b.putString("occasionId",occassion_id);
        b.putString("occasion",fragment_checkout_tv_occasion.getText().toString());

        b.putString("isNow",is_now);
        b.putString("personalMessage",fragment_checkout_et_message.getText().toString());
        b.putString("value_id",gift_amount_id);
        b.putString("value",fragment_checkout_tv_coupan_value.getText().toString());
        b.putString("rest_id",rest_id);
        b.putString("rest_name",rest_name);
        if(is_now.equalsIgnoreCase("1")){
          sendDate=dateSimpleDateFormat.format(date);
        }else{
            sendDate=fragment_checkout_rb_send_date.getText().toString(); }
        if(!TextUtils.isEmpty(imagePicker.getImagePath())){
            b.putString("imageFile",imagePicker.getImagePath());
        }else{
            b.putString("imagePath",imagePath);

        }
        b.putString("sendDate",sendDate);
        b.putString("occasionDate",sendDate);
        final CheckOutSummaryFragment checkoutSummaryFragment = new CheckOutSummaryFragment();
        checkoutSummaryFragment.setArguments(b);
        addFragment(CheckOutFragment.this,checkoutSummaryFragment,true);


    }

    private void setContactData() {
        final int finePermissionCheck = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_CONTACTS);
        if (finePermissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) ;
            {
                requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, Constant.REQUEST_CODE_ASK_PERMISSIONS);
            }
        } else {
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
            startActivityForResult(intent, REQUEST_CODE_PICK_CONTACTS);

        }
    }

    private void retrieveContactName() {
        final String contactName;
        String[] projection = {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER};
        Cursor c = getActivity().getContentResolver().query(uriContact, projection, null, null, null);
        c.moveToFirst();
        int nameIdx = c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        int phoneNumberIdx = c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
        contactName = c.getString(nameIdx);
        contactNo = c.getString(phoneNumberIdx);
        c.close();
        contactNo = contactNo.replaceAll("\\s","");
        fragment_checkout_tv_contactName.setText(Utils.getUpperCaseString(contactName));
    }

    private DatePickerDialog.OnDateSetListener onOccasionDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            dateTimeCalendar.set(Calendar.YEAR, year);
            dateTimeCalendar.set(Calendar.MONTH, month);
            dateTimeCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            if (dateTimeCalendar.compareTo(todayCalendar) < 0) {
                Logger.showSnackBar(getContext(), getString(R.string.not_select_less_than_todays_date));
            }
        }
    };
    private DatePickerDialog.OnDateSetListener onSendDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            dateTimeCalendar.set(Calendar.YEAR, year);
            dateTimeCalendar.set(Calendar.MONTH, month);
            dateTimeCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            if (dateTimeCalendar.compareTo(todayCalendar) < 0) {
                is_now="none";
                Logger.showSnackBar(getContext(), getString(R.string.not_select_less_than_todays_date));
            } else {
                is_now="0";
                fragment_checkout_rb_send_date.setText(dateSimpleDateFormat.format(dateTimeCalendar.getTime()));
            }
        }
    };


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.fragment_checkout_rb_select_date:
                is_now = "0";
                new DatePickerDialog(getContext(), onSendDateSetListener, dateTimeCalendar.get(Calendar.YEAR),
                        dateTimeCalendar.get(Calendar.MONTH), dateTimeCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.fragment_checkout_rb_now:
                fragment_checkout_rb_send_date.setText(getString(R.string.select_date));
                is_now = "1";
        }
    }
}
