package com.flunky.Fragments;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.flunky.Activities.MainActivity;
import com.flunky.R;
import com.flunky.utils.Logger;
import com.flunky.utils.Utils;

public class AboutFlunkyFragment extends BaseFragment implements View.OnClickListener {
    private TextView aboutFlunkyFragment_tv_version;
    private TextView aboutFlunkyFragment_tv_countact_us;
    private TextView aboutFlunkyFragment_tv_send_feedback;
    private TextView aboutFlunlyFragment_tv_rate_us;
    private TextView aboutFlunkyFragment_tv_flunky_insta;
    private TextView aboutFlubnkly_tv_flunky_fb;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       return inflater.inflate(R.layout.fragment_about_flunky,container,false);
    }

    @Override
    protected void initView(View view) {
        final String fb_text = "<font color=#00d17d>FLUNKY</font>  <font color=#ffffff>ON FACEBOOK</font>";
        final String insta_text = "<font color=#00d17d>FLUNKY</font>  <font color=#ffffff>ON INSTAGRAM</font>";
        aboutFlunkyFragment_tv_version=(TextView)view.findViewById(R.id.fragment_about_tv_version);
        aboutFlunkyFragment_tv_countact_us=(TextView)view.findViewById(R.id.fragment_about_tv_contact_us);
        aboutFlunkyFragment_tv_send_feedback=(TextView)view.findViewById(R.id.fragment_about_tv_feedback);
        aboutFlunlyFragment_tv_rate_us=(TextView)view.findViewById(R.id.fragment_about_tv_rate_us);
        aboutFlunkyFragment_tv_flunky_insta=(TextView)view.findViewById(R.id.fragment_about_tv_flunky_insta);
        aboutFlubnkly_tv_flunky_fb=(TextView)view.findViewById(R.id.fragment_about_tv_flunky_fb);

        aboutFlunkyFragment_tv_flunky_insta.setText(Html.fromHtml(insta_text));
        aboutFlubnkly_tv_flunky_fb.setText(Html.fromHtml(fb_text));

        aboutFlunkyFragment_tv_countact_us.setOnClickListener(this);
        aboutFlunkyFragment_tv_send_feedback.setOnClickListener(this);
        aboutFlunlyFragment_tv_rate_us.setOnClickListener(this);
        aboutFlunkyFragment_tv_flunky_insta.setOnClickListener(this);
        aboutFlubnkly_tv_flunky_fb.setOnClickListener(this);
        PackageInfo pinfo = null;
        try {
            pinfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            int versionNumber = pinfo.versionCode;
            aboutFlunkyFragment_tv_version.setText(getString(R.string.version_1_0_0)+" "+versionNumber);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void initToolbar() {

        MainActivity.getInstance().titleTextViewVisibility().setVisibility(View.VISIBLE);

        MainActivity.getInstance().setActionBarTitle(getString(R.string.about));
        MainActivity.getInstance().getRow_tool_bar_ll_menu().setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fragment_about_tv_contact_us:
                if(Utils.isMailClientPresent(getContext())){
                    final String []To={"yourvoucher@flunky.com"};
                    Intent voucheremailIntent = new Intent(Intent.ACTION_SEND);
                    voucheremailIntent.setType("text/plain");
                    voucheremailIntent.putExtra(Intent.EXTRA_EMAIL,To);
                    voucheremailIntent.putExtra(Intent.EXTRA_SUBJECT, "Contact For Voucher Information");
                    startActivity(voucheremailIntent);
                }else{
                    Toast.makeText(getContext(), R.string.email_client,Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.fragment_about_tv_feedback:
                if(Utils.isMailClientPresent(getContext())){
                    final String []To={"feedback@flunky.com"};
                    Intent feedbackemailIntent = new Intent(Intent.ACTION_SEND);
                    feedbackemailIntent.setType("text/plain");
                    feedbackemailIntent.putExtra(Intent.EXTRA_EMAIL,To);
                    feedbackemailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback To Flunky");
                    startActivity(feedbackemailIntent);
                }else{
                    Toast.makeText(getContext(), R.string.email_client,Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.fragment_about_tv_rate_us:
                openPlayStore();
                break;
            case R.id.fragment_about_tv_flunky_insta:
                if(isAppInstalled("com.instagram.android")){

                    openInstaPage();
                }else{

                    Intent viewWebPage = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://www.instagram.com/flunky_app/"));
                    startActivity(viewWebPage);
                }
                break;
            case R.id.fragment_about_tv_flunky_fb:
                if(isAppInstalled("com.facebook.katana")){
                    openFbPage();
                }else{
                    Intent viewWebPage = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://www.facebook.com/flunkyapp/"));
                    startActivity(viewWebPage);
                }
                break;
        }
    }
    private void openPlayStore(){
        final Uri uri = Uri.parse("market://details?id=" + getContext().getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + getContext().getPackageName())));
        }
    }
    private void openInstaPage(){
        Uri uri = Uri.parse("http://instagram.com/_u/flunky_app");
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);
        likeIng.setPackage("com.instagram.android");
        try {
            startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://instagram.com/flunky_app")));
        }
    }
    private Intent openFbPage(){
        try {
            getContext().getPackageManager().getPackageInfo("com.facebook.katana", 0);
            return new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/<1017318548374250>"));
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/flunkyapp/"));
        }
    }
    private boolean isAppInstalled(String packageName) {
        PackageManager pm = getActivity().getPackageManager();
        boolean installed = false;
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            installed = false;
        }
        return installed;
    }

}
