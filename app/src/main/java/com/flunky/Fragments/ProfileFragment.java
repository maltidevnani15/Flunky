package com.flunky.Fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flunky.Activities.MainActivity;
import com.flunky.Model.ResponseOfAllApiModel;
import com.flunky.Model.UserResponseModel;
import com.flunky.R;
import com.flunky.utils.Constant;
import com.flunky.utils.Logger;
import com.flunky.utils.Utils;
import com.flunky.webservices.RestClient;
import com.flunky.webservices.RetrofitCallback;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import retrofit2.Call;

public class ProfileFragment extends BaseFragment implements View.OnClickListener {
    private LinearLayout fragment_progile_ll_change_pwd;
    private UserResponseModel userModel;
    private EditText fragment_profile_et_fname;
    private EditText fragment_profile_et_lname;
    private TextView fragment_profile_tv_birthday;
    private EditText fragment_profile_et_pwd;
    private EditText fragment_profile_et_repwd;
    private TextView fragment_profile_tv_update;
    private TextView fragment_profile_tv_change_pwd;
    private Calendar dateTimeCalendar;
    private SimpleDateFormat simpleDateFormat;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    protected void initView(View view) {
        dateTimeCalendar = Calendar.getInstance();
        simpleDateFormat = new SimpleDateFormat(Constant.EXPECTED_DATE_FORMATTER, Locale.getDefault());
        final String userData = Utils.getString(getContext(), Constant.KEY_USER_DATA);
        if (!TextUtils.isEmpty(userData) && !userData.equalsIgnoreCase("null")) {
            userModel = new Gson().fromJson(userData, UserResponseModel.class);
        }
        fragment_progile_ll_change_pwd=(LinearLayout)view.findViewById(R.id.fragment_profile_ll_change_pwd);
        fragment_profile_et_fname = (EditText) view.findViewById(R.id.fragment_profile_et_fname);
        fragment_profile_et_lname = (EditText) view.findViewById(R.id.fragment_profile_et_lname);
        fragment_profile_et_pwd = (EditText) view.findViewById(R.id.fragment_profile_et_pwd);
        fragment_profile_et_repwd = (EditText) view.findViewById(R.id.fragment_profile_et_rpwd);

        fragment_profile_tv_birthday = (TextView) view.findViewById(R.id.fragment_profile_et_birthday);
        fragment_profile_tv_update = (TextView) view.findViewById(R.id.fragment_profile_tv_submit);
        fragment_profile_tv_change_pwd = (TextView) view.findViewById(R.id.fragment_profile_tv_change_pwd);


        fragment_profile_tv_birthday.setOnClickListener(this);
        fragment_profile_tv_update.setOnClickListener(this);
        fragment_profile_tv_change_pwd.setOnClickListener(this);
        if(userModel.getUserAccountType().equalsIgnoreCase("0")){
            fragment_progile_ll_change_pwd.setVisibility(View.VISIBLE);
        }else{
            fragment_progile_ll_change_pwd.setVisibility(View.GONE);
        }
        setData(userModel);

    }

    @Override
    protected void initToolbar() {
        MainActivity.getInstance().titleTextViewVisibility().setVisibility(View.VISIBLE);
        MainActivity.getInstance().setActionBarTitle(getString(R.string.my_profile));
        MainActivity.getInstance().getRow_tool_bar_ll_menu().setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_profile_et_birthday:
                String input = userModel.getBirth_date();
                String[] out = input.split("/");
                new DatePickerDialog(getContext(), onDateChangeListener, Integer.parseInt(out[2]),
                        Integer.parseInt(out[1]) - 1, Integer.parseInt(out[0])).show();
                break;
            case R.id.fragment_profile_tv_submit:
                Utils.hideSoftKeyboard(getActivity());
                doValidation();
                break;
            case R.id.fragment_profile_tv_change_pwd:
                Utils.hideSoftKeyboard(getActivity());
                doChangePasswordValidation();
                break;
        }
    }

    private void doChangePasswordValidation() {
        final String pwd = fragment_profile_et_pwd.getText().toString();
        final String rePwd = fragment_profile_et_repwd.getText().toString();
        if (!TextUtils.isEmpty(pwd)) {
            if (pwd.length() > 4) {
                if (!TextUtils.isEmpty(rePwd) && pwd.equalsIgnoreCase(rePwd)) {
                    changePassword(pwd);
                } else {
                    Logger.showSnackBar(getContext(), getString(R.string.repeat_pawd_not_match));
                }
            } else {
                Logger.showSnackBar(getContext(), getString(R.string.password_validation));
            }

        } else {
            Logger.showSnackBar(getContext(), getString(R.string.enter_password));
        }
    }

    private void changePassword(String pwd) {
        Call<JsonObject>changepasswordCall = new RestClient().getApiInterface().changePassword(Constant.APP_KEY,userModel.getUser_id(),pwd,Constant.OS);
        changepasswordCall.enqueue(new RetrofitCallback<JsonObject>(getContext(),Logger.showProgressDialog(getContext())) {
            @Override
            public void onSuccess(JsonObject data) {
                if(data.get("res_code").getAsString().equalsIgnoreCase("1")){
                    Logger.toast(getContext(),data.get("res_message").toString());
                }else{
                    Logger.toast(getContext(),"Not able to change the password try again!!!");
                }

            }
        });
    }

    private void doValidation() {
        final String fname = fragment_profile_et_fname.getText().toString();
        final String lname = fragment_profile_et_lname.getText().toString();
        final String birthDate = fragment_profile_tv_birthday.getText().toString();
        if (!TextUtils.isEmpty(fname)) {
            if (!TextUtils.isEmpty(lname)) {
                doUpdate(fname, lname, birthDate);
            }else {
                Logger.showSnackBar(getContext(),getString(R.string.enter_last_name));
            }
        }else {
            Logger.showSnackBar(getContext(),getString(R.string.enter_name));
        }
    }

    private void doUpdate(String fname, String lname, String birthDate) {
        Call<ResponseOfAllApiModel> update = new RestClient().getApiInterface().updateProfile(Constant.APP_KEY, userModel.getUser_id(), fname, lname, birthDate,Constant.OS);
        update.enqueue(new RetrofitCallback<ResponseOfAllApiModel>(getContext(), Logger.showProgressDialog(getContext())) {
            @Override
            public void onSuccess(ResponseOfAllApiModel data) {
                Logger.toast(getContext(), getString(R.string.progile_msg));
                final UserResponseModel userResponseModel = data.getUserResponseModel();
                Utils.storeString(getContext(), Constant.KEY_USER_DATA, new Gson().toJson(userResponseModel));
                setData(userResponseModel);
            }
        });
    }

    private void setData(UserResponseModel userModel) {
        fragment_profile_et_fname.setText(Utils.getUpperCaseString(userModel.getFirst_name()));
        fragment_profile_et_lname.setText(Utils.getUpperCaseString(userModel.getLast_name()));
        fragment_profile_tv_birthday.setText(userModel.getBirth_date());
    }

    private DatePickerDialog.OnDateSetListener onDateChangeListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            dateTimeCalendar.set(Calendar.YEAR, year);
            dateTimeCalendar.set(Calendar.MONTH, month);
            dateTimeCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            fragment_profile_tv_birthday.setText(simpleDateFormat.format(dateTimeCalendar.getTime()));

        }
    };
}
