package com.flunky.Activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.flunky.Model.UserModel;
import com.flunky.R;
import com.flunky.utils.Constant;
import com.flunky.utils.Logger;
import com.flunky.utils.Utils;
import com.flunky.webservices.RestClient;
import com.flunky.webservices.RetrofitCallback;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import retrofit2.Call;


public class SignUpActivity extends BaseActivity implements View.OnClickListener {
     private EditText signup_et_fname;
    private EditText signup_et_lname;
    private TextView signup_et_birthday;
    private EditText signup_et_email;
    private EditText signup_et_mobile;
    private EditText signup_et_pwd;
    private EditText signup_et_rpwd;

    private TextView signup_tv_submit;
    private UserModel userModel;
    private Calendar dateTimeCalendar;
    private SimpleDateFormat simpleDateFormatter;
    private String userAccountType="0";
    private String fbid="";
    @Override
    protected void initView() {
        dateTimeCalendar=Calendar.getInstance();
        simpleDateFormatter=new SimpleDateFormat(Constant.EXPECTED_DATE_FORMATTER, Locale.getDefault());
        signup_et_fname =(EditText)findViewById(R.id.signup_et_fname);
        signup_et_lname = (EditText)findViewById(R.id.signup_et_lname);
        signup_et_email = (EditText)findViewById(R.id.signup_et_email);
        signup_et_birthday=(TextView)findViewById(R.id.signup_et_birthday);
        signup_et_mobile = (EditText)findViewById(R.id.signup_et_mobile);
        signup_et_pwd = (EditText)findViewById(R.id.signup_et_pwd);
        signup_et_rpwd = (EditText)findViewById(R.id.signup_et_rpwd);
        signup_tv_submit = (TextView)findViewById(R.id.signup_tv_submit);

        signup_et_birthday.setOnClickListener(this);
        signup_tv_submit.setOnClickListener(this);

        final String userData = Utils.getString(this, Constant.KEY_DATA);
        if (!TextUtils.isEmpty(userData) && !userData.equalsIgnoreCase("null")) {
            userModel = new Gson().fromJson(userData, UserModel.class);
//            doSignUp(userModel.getFirstName(),userModel.getLastName(),userModel.getEmail(),userModel.getMobile(),userModel.getBirth_date(),
//                    userModel.getPasword(),userModel.getPass_confirm(),userModel.getApiKey(),"1",
//                    userModel.getFacebookId());
           setSignUpDataForFbUser(userModel);

            Log.e("name",userModel.getFirstName());
        }



    }

    private void setSignUpDataForFbUser(UserModel userModel) {

        signup_et_fname.setText(userModel.getFirstName().toString());
        signup_et_lname.setText(userModel.getLastName());
        signup_et_email.setText(userModel.getEmail());
        signup_et_pwd.setVisibility(View.GONE);
        signup_et_rpwd.setVisibility(View.GONE);
        userAccountType="1";
        fbid=userModel.getFacebookId();

    }

    @Override
    protected void initToolBar() {

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_sign_up;
    }

    @Override
    public void onClick(View v) {

        int id=v.getId();
        switch (id){
            case R.id.signup_tv_submit:
                Utils.hideSoftKeyboard(this);
                validateData();
                break;
            case R.id.signup_et_birthday:
                new DatePickerDialog(this,onBirthdaySelectListner,1990,0,1).show();
        }
    }
    private DatePickerDialog.OnDateSetListener onBirthdaySelectListner=new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            dateTimeCalendar.set(Calendar.YEAR, year);
            dateTimeCalendar.set(Calendar.MONTH, month);
            dateTimeCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            signup_et_birthday.setText(simpleDateFormatter.format(dateTimeCalendar.getTime()));
        }
    };

    private void validateData() {
        final String name = signup_et_fname.getText().toString();
        final String last_name=signup_et_lname.getText().toString();
        final String birthday = signup_et_birthday.getText().toString();
        final String email= signup_et_email.getText().toString();
        final String mobile = signup_et_mobile.getText().toString();
        final String pwd=signup_et_pwd.getText().toString();
        final String rpwd=signup_et_rpwd.getText().toString();
        if(!TextUtils.isEmpty(name)){
            if(!TextUtils.isEmpty(last_name)){
                if(!TextUtils.isEmpty(birthday)){
                    if(!TextUtils.isEmpty(email)&& Utils.isEmailValid(email)){
                        if(!TextUtils.isEmpty(mobile)){
                            if(signup_et_pwd.getVisibility()==View.VISIBLE){
                                if(!TextUtils.isEmpty(pwd)){
                                    if(pwd.length()>=4){
                                        if(!TextUtils.isEmpty(rpwd) && pwd.equalsIgnoreCase(rpwd)){
                                            doSignUp(name,last_name,email,mobile,birthday,pwd,rpwd,Constant.APP_KEY,userAccountType,fbid);
                                        }else{
                                            Logger.showSnackBar(this,getString(R.string.repeat_pawd_not_match));
                                        }
                                    }else{
                                        Logger.showSnackBar(this,getString(R.string.password_validation));
                                    }

                                }else{
                                    Logger.showSnackBar(this,getString(R.string.enter_password));
                                }
                            }else{
                                doSignUp(name,last_name,email,mobile,birthday,pwd,rpwd,Constant.APP_KEY,userAccountType,fbid);
                            }

                        }else{
                            Logger.showSnackBar(this,getString(R.string.enter_mobile));
                        }

                    }else{
                        Logger.showSnackBar(this,getString(R.string.enter_valid_email));
                    }
                }else{
                    Logger.showSnackBar(this, getString(R.string.enter_birthdate));
                }

            }else {
                Logger.showSnackBar(this,getString(R.string.enter_last_name));
            }
        }else{
            Logger.showSnackBar(this,getString(R.string.enter_name));
        }
        
    }

    public void doSignUp(final String name, final String last_name, final String email, final String mobile, final String birthday, final String pwd, final String rpwd, final String apiKey, final String userAccount, final String fbId) {
        final Call<JsonObject> signUpCall = RestClient.getInstance().getApiInterface().
               doSignUp(email,name,last_name,birthday,mobile,pwd,rpwd,apiKey,userAccount,fbId,Constant.OS);
        signUpCall.enqueue(new RetrofitCallback<JsonObject>(this, Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(JsonObject data)
            {
                Utils.storeString(SignUpActivity.this,Constant.KEY_DATA,"");
                Logger.toast(SignUpActivity.this,data.get("res_message").toString());
                if(data.get("res_code").getAsString().equalsIgnoreCase("0")){
                    Logger.toast(SignUpActivity.this,getString(R.string.email_signUp_error));
                }else{

                        Intent  i = new Intent(SignUpActivity.this,LoginActivity.class);
                        navigateToNextActivity(i,true);

                }
            }
        });
//
    }
}
