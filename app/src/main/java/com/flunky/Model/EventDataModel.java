package com.flunky.Model;

import com.google.gson.annotations.SerializedName;

public class EventDataModel {
    @SerializedName("cal_id")
    private String cal_id;
    @SerializedName("cal_user_id")
    private String cal_user_id;
    @SerializedName("cal_event_name")
    private String event_name;
    @SerializedName("cal_event_occasion")
    private String event_occasion;
    @SerializedName("cal_event_desc")
    private String event_desc;
    @SerializedName("cal_event_date")
    private String event_date;

    public String getCal_id() {
        return cal_id;
    }

    public void setCal_id(String cal_id) {
        this.cal_id = cal_id;
    }

    public String getCal_user_id() {
        return cal_user_id;
    }

    public void setCal_user_id(String cal_user_id) {
        this.cal_user_id = cal_user_id;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEvent_occasion() {
        return event_occasion;
    }

    public void setEvent_occasion(String event_occasion) {
        this.event_occasion = event_occasion;
    }

    public String getEvent_desc() {
        return event_desc;
    }

    public void setEvent_desc(String event_desc) {
        this.event_desc = event_desc;
    }

    public String getEvent_date() {
        return event_date;
    }

    public void setEvent_date(String event_date) {
        this.event_date = event_date;
    }

    public String getNotify() {
        return notify;
    }

    public void setNotify(String notify) {
        this.notify = notify;
    }

    @SerializedName("cal_notify")
    private String notify;

}
