package com.flunky.Model;

import com.google.gson.annotations.SerializedName;

public class UserModel {
    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;


    @SerializedName("birth_date")
    private String birth_date;

    @SerializedName("email")
    private String email;

    @SerializedName("mobile")
    private String mobile;

    @SerializedName("password")
    private String pasword;

    @SerializedName("pass_confirm")
    private String pass_confirm;

    @SerializedName("apikey")
    private String apiKey;

    @SerializedName("useraccounttype")
    private int userAccountType;

    @SerializedName("facebookid")
    private String facebookId;

    @SerializedName("deviceid")
    private String deviceid;

    @SerializedName("devicetoken")
    private String devicetoken;

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }


    public String getDevicetoken() {
        return devicetoken;
    }

    public void setDevicetoken(String devicetoken) {
        this.devicetoken = devicetoken;
    }



    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public int getUserAccountType() {
        return userAccountType;
    }

    public void setUserAccountType(int userAccountType) {
        this.userAccountType = userAccountType;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPasword() {
        return pasword;
    }

    public void setPasword(String pasword) {
        this.pasword = pasword;
    }

    public String getPass_confirm() {
        return pass_confirm;
    }

    public void setPass_confirm(String pass_confirm) {
        this.pass_confirm = pass_confirm;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }



    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }




}
