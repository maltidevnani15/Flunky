package com.flunky.Model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class RestaurantRecommendationResponseModel implements Parcelable {
    @SerializedName("title")
    private String title;

    @SerializedName("desc")
    private String desc;

    @SerializedName("price")
    private String price;

    protected RestaurantRecommendationResponseModel(Parcel in) {
        title = in.readString();
        desc = in.readString();
        price = in.readString();
    }

    public static final Creator<RestaurantRecommendationResponseModel> CREATOR = new Creator<RestaurantRecommendationResponseModel>() {
        @Override
        public RestaurantRecommendationResponseModel createFromParcel(Parcel in) {
            return new RestaurantRecommendationResponseModel(in);
        }

        @Override
        public RestaurantRecommendationResponseModel[] newArray(int size) {
            return new RestaurantRecommendationResponseModel[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(desc);
        dest.writeString(price);
    }
}
