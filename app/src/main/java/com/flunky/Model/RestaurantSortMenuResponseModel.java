package com.flunky.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RestaurantSortMenuResponseModel {
    @SerializedName("rest_location")
    private ArrayList<RestaurantSortMenuItemsModel> restaurantSortMenulocationData;

    @SerializedName("rest_cusine")
    private ArrayList<RestaurantSortMenuItemsModel> restaurantSortMenuCusineData;

    @SerializedName("rest_cost")
    private ArrayList<RestaurantSortMenuItemsModel> restaurantSortMenuCostData;

    @SerializedName("coupon_price")
    private ArrayList<RestaurantSortMenuItemsModel> restaurantSortMenuCoupanPrice;

    @SerializedName("rest_event")
    private ArrayList<RestaurantSortMenuItemsModel> restaurantSortMenuEvents;

    public ArrayList<RestaurantSortMenuItemsModel> getRestaurantSortMenuCoupanPrice() {
        return restaurantSortMenuCoupanPrice;
    }

    public void setRestaurantSortMenuCoupanPrice(ArrayList<RestaurantSortMenuItemsModel> restaurantSortMenuCoupanPrice) {
        this.restaurantSortMenuCoupanPrice = restaurantSortMenuCoupanPrice;
    }

    public ArrayList<RestaurantSortMenuItemsModel> getRestaurantSortMenuEvents() {
        return restaurantSortMenuEvents;
    }

    public void setRestaurantSortMenuEvents(ArrayList<RestaurantSortMenuItemsModel> restaurantSortMenuEvents) {
        this.restaurantSortMenuEvents = restaurantSortMenuEvents;
    }



    public ArrayList<RestaurantSortMenuItemsModel> getRestaurantSortMenulocationData() {
        return restaurantSortMenulocationData;
    }

    public void setRestaurantSortMenulocationData(ArrayList<RestaurantSortMenuItemsModel> restaurantSortMenulocationData) {
        this.restaurantSortMenulocationData = restaurantSortMenulocationData;
    }

    public ArrayList<RestaurantSortMenuItemsModel> getRestaurantSortMenuCusineData() {
        return restaurantSortMenuCusineData;
    }

    public void setRestaurantSortMenuCusineData(ArrayList<RestaurantSortMenuItemsModel> restaurantSortMenuCusineData) {
        this.restaurantSortMenuCusineData = restaurantSortMenuCusineData;
    }

    public ArrayList<RestaurantSortMenuItemsModel> getRestaurantSortMenuCostData() {
        return restaurantSortMenuCostData;
    }

    public void setRestaurantSortMenuCostData(ArrayList<RestaurantSortMenuItemsModel> restaurantSortMenuCostData) {
        this.restaurantSortMenuCostData = restaurantSortMenuCostData;
    }



}
