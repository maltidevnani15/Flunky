package com.flunky.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import com.flunky.Interfaces.OnItemCheckChangeListener;
import com.flunky.Model.RestaurantSortMenuItemsModel;
import com.flunky.R;
import java.util.ArrayList;

public class RestaurantSortMenuItemsAdapter extends RecyclerView.Adapter<RestaurantSortMenuItemsAdapter.ViewHolder>  {
    private Context context;
    private OnItemCheckChangeListener onItemCheckChangeListener;

    private ArrayList<RestaurantSortMenuItemsModel>restaurantSortMenuItemsModels;
    private LayoutInflater inflater;
    public RestaurantSortMenuItemsAdapter(Context context, ArrayList<RestaurantSortMenuItemsModel>restaurantSortMenuItemsModels){
        this.context=context;
        this.restaurantSortMenuItemsModels = restaurantSortMenuItemsModels;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {final View view = inflater.inflate(R.layout.row_tabs_items,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final RestaurantSortMenuItemsModel sortMenuItemsModel = restaurantSortMenuItemsModels.get(position);
        if(sortMenuItemsModel.is_checked()){
            holder.restaurant_sort_menu_item_cb_cusine.setTextColor(context.getResources().getColor(R.color.light_green));

        }else{
            holder.restaurant_sort_menu_item_cb_cusine.setTextColor(context.getResources().getColor(R.color.white));

        }
        holder.restaurant_sort_menu_item_cb_cusine.setText(sortMenuItemsModel.getValue().toLowerCase());

    }

    @Override
    public int getItemCount() {
        return restaurantSortMenuItemsModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {

        private CheckBox restaurant_sort_menu_item_cb_cusine;

        public ViewHolder(final View itemView) {
            super(itemView);

            restaurant_sort_menu_item_cb_cusine = (CheckBox) itemView.findViewById(R.id.row_tabs_item_cb_cusine);
            restaurant_sort_menu_item_cb_cusine.setOnCheckedChangeListener(this);

        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(onItemCheckChangeListener!=null)
                onItemCheckChangeListener.onItemCheck(getAdapterPosition(),isChecked,itemView);
        }
    }
    public void setOnCheckItemListener(OnItemCheckChangeListener onItemCheckChangeListener){
        this.onItemCheckChangeListener=onItemCheckChangeListener;
    }
}
