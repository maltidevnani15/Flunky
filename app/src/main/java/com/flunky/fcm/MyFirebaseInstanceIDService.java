package com.flunky.fcm;

import android.app.Service;
import android.util.Log;

import com.flunky.utils.Constant;
import com.flunky.utils.Logger;
import com.flunky.utils.Utils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
     @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Utils.storeString(this, Constant.GCM_UNIQUE_ID,refreshedToken);
        Log.e("deviceToken",refreshedToken);
    }
}
